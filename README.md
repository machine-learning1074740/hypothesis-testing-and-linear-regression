# Linear Regression Analysis in Python

This project is dedicated to performing a linear regression analysis in Python using various libraries and tools. We'll be exploring the linear regression model, assessing its significance, and visualizing the results. Below is an overview of the modules and data used in this project:

## Used Modules

We make use of the following Python modules in this project:

- `sklearn.datasets`: To generate synthetic data for the regression analysis.
- `scipy.stats`: For statistical computations, including t-distribution.
- `pandas`: To handle and manipulate the dataset.
- `numpy`: For numerical operations.
- `tabulate`: To display data in a tabular format.
- `matplotlib.pyplot`: For data visualization.

## Used Data

We generate synthetic data for this analysis:

```python
# Generating synthetic data
from sklearn.datasets import make_regression
from scipy import stats
from scipy.stats import t
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

n = 240  # Number of data points
X, Y = make_regression(n_samples=n, n_features=1, noise=5)
a = np.array(abs(X))
b = np.array(abs(Y.reshape(n, 1))
df = np.concatenate((a, b), axis=1)
data = pd.DataFrame(df, columns=['X', 'Y'])
result = stats.linregress(df)
```

## Analysis Steps

1. **Defining the Linear Model**:
We define the linear regression model as `lr = a * slope + intercept`.

2. **Compute 'y hat'**:
We compute the predicted values (`y hat`) from the simple linear model.

3. **Compute Residuals**:
We calculate the differences between actual `Y` values and predicted `Y` values, followed by squaring these differences.

4. **Residual Squared Sum (RSS)**:
We sum the squared residuals to obtain the Residual Squared Sum.

5. **Estimation of Sigma Squared**:
We estimate the variance `sigma squared` from the RSS.

6. **T-Value and Significance Test**:
We calculate the T-value for the intercept, compare it to the critical value from the Student t-distribution, and determine the significance of the estimate.

7. **Plotting the T-Value Distribution**:
We create a plot showing the distribution of T-values for the intercept and compare it to the calculated T-value.

These steps are performed to assess the linear regression model's significance and visualize the results.

The project includes code snippets, data visualizations, and statistical analysis.